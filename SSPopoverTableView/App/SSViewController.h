//
//  SSViewController.h
//  SSPopoverTableView
//
//  Created by James Van Dyne on 12/20/13.
//  Copyright (c) 2013 Sugoi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSViewController : UITableViewController


@property IBOutlet UIBarButtonItem *barButtonItem;

@end
